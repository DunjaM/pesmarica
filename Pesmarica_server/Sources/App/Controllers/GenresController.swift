import Vapor
import FluentProvider

struct GenresController {
    
    // MARK:- Add to droplet
    
    func addRoutes(to drop: Droplet) {
        
        let genres = drop.grouped("genres")
        genres.get(handler: allGenres)
        genres.post("create", handler: createGenre)
        genres.get(Genre.parameter, handler: getGenre)
        genres.get(Genre.parameter, "songs", handler: getGenreSongs)
    }
    
    // MARK:- Request
    
    func allGenres(_ request: Request) throws -> ResponseRepresentable {
        let genres = try Genre.all()
        return try genres.makeJSON()
    }
    
    func createGenre(_ request: Request) throws -> ResponseRepresentable {
        guard let json = request.json else {
            throw Abort.badRequest
        }
        
        let genre = try Genre(json: json)
        try genre.save()
        
        return genre
    }
    
    func getGenre(_ request: Request) throws -> ResponseRepresentable {
        let genre = try request.parameters.next(Genre.self)
        return genre
    }
    
    func getGenreSongs(_ request: Request) throws -> ResponseRepresentable {
        let genre = try request.parameters.next(Genre.self)
        return try genre.songs.all().makeJSON()
    }
}
