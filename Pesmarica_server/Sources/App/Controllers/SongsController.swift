
import Vapor
import FluentProvider

struct SongsController {
    
    // MARK:- Add to droplet
    
    func addRoutes(to drop: Droplet) {
        
        let songs = drop.grouped("songs")
        
        songs.get(handler: allSongs)
        songs.post("create", handler: createSong)
        songs.get(Song.parameter, handler: getSong)
        songs.get(Song.parameter, "genre", handler: getSongGenre)
    }
    
    // MARK:- Requests
    
    func allSongs(_ request: Request) throws -> ResponseRepresentable {
        let allSongs = try Song.all()
        return try allSongs.makeJSON()
    }
    
    func createSong(_ request: Request) throws -> ResponseRepresentable {
        guard let json = request.json else {
            throw Abort.badRequest
        }
        
        let song = try Song(json: json)
        
        if try songIsValid(title: song.title, singer: song.singer) {
            try song.save()
        } else {
            return "Vec smo upisali tu pesmu"
        }
        
        
        return song
    }
    
    func removeSong(_ request: Request) throws -> ResponseRepresentable {
        let song = try request.parameters.next(Song.self)
        try song.delete()
        return song
    }
    
    func getSong(_ request: Request) throws -> ResponseRepresentable {
        let song = try request.parameters.next(Song.self)
        return song
    }
    
    func getSongGenre(_ request: Request) throws -> ResponseRepresentable {
        let song = try request.parameters.next(Song.self)
        
        guard let genre = try song.genre.get() else {
            throw Abort.notFound
        }
        
        return genre
    }
    
    func songIsValid(title: String, singer: String) throws -> Bool {
        let allSongs = try Song.all()
        
        let sameSongTitles = allSongs.filter { $0.title == title }
        let completelySameSongs = sameSongTitles.filter { $0.singer == singer }
        
        let valid = completelySameSongs.isEmpty
        
        return valid
    }
}
