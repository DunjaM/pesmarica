
import FluentProvider

// MARK:- Model

final class Song: Model, JSONConvertible {
    
    let storage = Storage()
    
    let title: String
    let singer: String
    
    let genreId: Identifier?
    
    init(singer: String, title: String, genre: Genre) {
        self.title = title
        self.singer = singer
        self.genreId = genre.id
    }
    
    // Row
    
    init(row: Row) throws {
        title = try row.get("title")
        singer = try row.get("singer")
        genreId = try row.get(Genre.foreignIdKey)
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("title", title)
        try row.set("singer", singer)
        try row.set(Genre.foreignIdKey, genreId)
        return row
    }
    
    // JSON
    
    convenience init(json: JSON) throws {
        let genreId: Identifier = try json.get("genre_id")
        guard let genre = try Genre.find(genreId) else {
            throw Abort.badRequest
        }
        try self.init(singer: json.get("singer"), title: json.get("title"), genre: genre)
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("title", title)
        try json.set("singer", singer)
        try json.set("genre_id", genreId)
        return json
    }
}

// MARK:- Database

extension Song: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { song in
            song.id()
            song.string("title")
            song.string("singer")
            song.parent(Genre.self)
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK:- Response

extension Song: ResponseRepresentable { }

// MARK:- Relationships

extension Song {
    var genre: Parent<Song, Genre> {
        return parent(id: genreId)
    }
}

