
import FluentProvider

// MARK:- Model

final class Genre: Model, JSONConvertible {
    
    let storage = Storage()
    
    let name: String
    
    init(name: String) {
        self.name = name
    }
    
    // Row
    
    init(row: Row) throws {
        name = try row.get("name")
    }
    
    func makeRow() throws -> Row {
        var row = Row()
        try row.set("name", name)
        return row
    }
    
    // JSON
    
    convenience init(json: JSON) throws {
        try self.init(name: json.get("name"))
    }
    
    func makeJSON() throws -> JSON {
        var json = JSON()
        try json.set("id", id)
        try json.set("name", name)
        return json
    }
}

// MARK:- Database

extension Genre: Preparation {
    static func prepare(_ database: Database) throws {
        try database.create(self) { song in
            song.id()
            song.string("name")
        }
    }
    
    static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}

// MARK:- Response

extension Genre: ResponseRepresentable { }

// MARK:- Relationships

extension Genre {
    var songs: Children<Genre, Song> {
        return children()
    }
}


