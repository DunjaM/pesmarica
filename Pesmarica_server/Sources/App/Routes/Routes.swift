import Vapor

extension Droplet {
    func setupRoutes() throws {
        
        let songsController = SongsController()
        songsController.addRoutes(to: self)
        
        let genresController = GenresController()
        genresController.addRoutes(to: self)
    }
}
