//
//  Genre.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

struct GenreJSON: Decodable {
    
    let id: Int
    let name: String
}
