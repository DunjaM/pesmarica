//
//  Song.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

struct SongJSON: Decodable {
    
    let id: Int?
    let genre_id: Int
    let singer: String
    let title: String
    
    init(singer: String, title: String, genre_id: Int) {
        self.singer = singer
        self.title = title
        self.genre_id = genre_id
        self.id = nil
    }
}
