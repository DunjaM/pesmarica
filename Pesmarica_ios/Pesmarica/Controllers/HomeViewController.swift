//
//  ViewController.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/5/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import UIKit
import CoreData

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Outlets

    @IBOutlet weak var genrePicker: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searxhTopConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    
    var songs: [Song] = [] {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.genrePicker.alpha = 1
            }
        }
    }
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configView()
    }
    
    func configView() {
        
        genrePicker.alpha = 0
        
        GenresAPI.configGenrePicker(genrePicker)
        
        getSongsForChosenGenre()
    }
    
    // MARK: - API
    
    func getSongsForChosenGenre() {
        
        let genreId = genrePicker.selectedSegmentIndex
        
        SongsAPI.findSongs(ofGenre: genreId) { (songs) in
            self.songs = songs
        }
    }
    
    // MARK: - Table view
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell", for: indexPath)
        
        cell.textLabel?.text = songs[indexPath.row].title
        cell.detailTextLabel?.text = songs[indexPath.row].singer
        
        return cell
    }
    
    // MARK: - Actions
    
    @IBAction func genreChanged(_ sender: UISegmentedControl) {
        
        getSongsForChosenGenre()
    }
    
    @IBAction func addNewSong() {
        
        if let addNewModal = storyboard?.instantiateViewController(withIdentifier: "NewSongModalViewController") as? NewSongModalViewController {
            addNewModal.modalPresentationStyle = .overCurrentContext
            present(addNewModal, animated: true)
            
            addNewModal.onSave = { song in
                
                SongsAPI.addNewSong(song, completion: { (song, success) in
                    if success {
                        
                        if let song = song {
                            
                            print("Pesma \(song.title!) je uspesno sacuvana")
                            
                            self.songs.append(song)
                        } else {
                            print("error")
                        }
                    }
                })
                
            }
        }
    }
    
    @IBAction func getRandomSong() {
        
        if let song = PersistenceService.getRandomSong(ofGenre: nil) {
            let dialog = UIAlertController(title: "Predlog", message: "🎶\n\(song.singer ?? "") - \(song.title ?? "")", preferredStyle: .alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            dialog.addAction(ok)
            
            present(dialog, animated: true)
        }
    }
}

