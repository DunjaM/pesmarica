//
//  NewSongModalViewController.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import UIKit

class NewSongModalViewController: UIViewController, UITextFieldDelegate {

    // MARK: - Outlets
    
    @IBOutlet weak var modalView: UIView!
    @IBOutlet weak var singerTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var genrePicker: UISegmentedControl!
    
    // MARK: - Variables
    
    var songSinger = ""
    var songTitle = ""
    
    var onSave: ((_ song: SongJSON) -> ())?
    
    // MARK: - View methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configGenres()
        
        modalView.layer.cornerRadius = 10
    }
    
    // MARK: - API
    
    func configGenres() {
        
        GenresAPI.configGenrePicker(genrePicker)
    }

    // MARK: - Text field
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == singerTextField {
            titleTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func allFieldsValid() -> Bool {
        
        songSinger = singerTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        songTitle = titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""

        if songSinger.isEmpty || songTitle.isEmpty {
            return false
        }
        return true
    }
    
    // MARK: - Actions
    
    @IBAction func okButtonAction() {
        
        if allFieldsValid() {
            
            let genreId = genrePicker.selectedSegmentIndex + 1
            
            let song = SongJSON(singer: songSinger, title: songTitle, genre_id: genreId)
            onSave?(song)
            dismiss(animated: true)
        }
    }
    
    @IBAction func closeAction() {
        dismiss(animated: true)
    }
}
