//
//  PersistenceService.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import UIKit
import CoreData

class PersistenceService: NSObject {
    
    static var context: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    static var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Model")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    static func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK:- Fetching
    
    static func fetchAllGenres() -> [Genre]? {
        
        let genreRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Genre")

        do {
            let fetchedGenres = try context.fetch(genreRequest) as? [Genre]

            return fetchedGenres
        } catch {
            print("error fetching genres")

            return nil
        }
    }
    
    static func fetchSongs(ofGenre genreId: Int?) -> [Song]? {
        
        let songRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Song")
        
        if genreId != nil {
            let predicateString = "genre_id = \(Int16(genreId! + 1))"
            
            songRequest.predicate = NSPredicate(format: predicateString)
        }
       
        do {
            let fetchedSongs = try context.fetch(songRequest) as? [Song]
            return fetchedSongs
        } catch {
            print("error fetching genres")
            return nil
        }
    }
    
    static func getRandomSong(ofGenre genreId: Int?) -> Song? {

        if let allSongsOfGenre = fetchSongs(ofGenre: genreId), allSongsOfGenre.count > 0 {
            let randomIndex = Int(arc4random() % UInt32(allSongsOfGenre.count))
            
            return allSongsOfGenre[randomIndex]
        }
        
        return nil
    }
}
