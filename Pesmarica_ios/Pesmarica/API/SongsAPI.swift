//
//  SongsAPI.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import Foundation

class SongsAPI: BaseAPI {
    
    static let create = "/create"
    
    static func getAllSongs(completion: @escaping (_ genres: [Song]?) -> Void) {
        
        sendRequest(Group.songs, method: .get, params: nil) { (data, success) in
            
            if success {
                do {
                    let songsJSON = try JSONDecoder().decode([SongJSON].self, from: data!)
                    let songs = saveSongToCoreData(songsJSON)
                    
                    completion(songs)
                } catch {
                    
                }
            } else {
                // Handle error
            }
        }
    }
    
    static func addNewSong(_ song: SongJSON, completion: @escaping (_ song: Song?, _ success: Bool) -> Void) {
        
        let jsonParams = ["singer" : song.singer,
                      "title" : song.title,
                      "genre_id" : song.genre_id] as Dictionary<String, Any>
        
        sendRequest(Group.songs + create, method: .post, params: jsonParams) { (data, success) in
            if success {
                do {
                    let songJSON = try JSONDecoder().decode(SongJSON.self, from: data!)
                    let song = saveSongToCoreData([songJSON])
                    
                    completion(song?.first, true)
                } catch {
                    completion(nil, false)
                }
            } else {
                // Handle error
                completion(nil, false)
            }
        }
    }
    
    // MARK:- Core Data
    
    static func saveSongToCoreData(_ songs: [SongJSON]) -> [Song]? {
        
        var savedSongs = [Song]()
        
        for song in songs {
            let newSong = Song(context: PersistenceService.context)
            newSong.title = song.title
            newSong.singer = song.singer
            newSong.song_id = Int16(song.id!)
            newSong.genre_id = Int16(song.genre_id)
            
            savedSongs.append(newSong)
        }
        
        do {
            try PersistenceService.context.save()
            
            return savedSongs
        } catch {
            print("Saving error")
            
            return nil
        }
    }
    
    static func findSongs(ofGenre genreId: Int, completion: @escaping ([Song]) -> ()) {
        
        let fetchedSongs = PersistenceService.fetchSongs(ofGenre: genreId)
        
        if fetchedSongs == nil || fetchedSongs!.isEmpty {
            
            // TODO //
            // Get songs for selected genre
            
            getAllSongs(completion: { (songs) in
                print("songs from server")
                
                if let songs = songs {
                    completion(songs)
                }
            })
        } else {
            completion(fetchedSongs!)
        }
    }
}
