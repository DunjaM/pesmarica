//
//  GenresAPI.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/6/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import Foundation
import UIKit

class GenresAPI: BaseAPI {
    
    static func getAllGenres(completion: @escaping (_ genres: [Genre]?) -> Void) {
        
        sendRequest(Group.genres, method: .get, params: nil) { (data, success) in
            
            if success {
                do {
                    let genresJSON = try JSONDecoder().decode([GenreJSON].self, from: data!)
                    let genres = saveGenresToCoreData(genresJSON)
                    
                    completion(genres)
                } catch {
                    
                }
            } else {
                // Handle error
            }
        }
    }
    
    // MARK:- Core Data
    
    static func saveGenresToCoreData(_ genres: [GenreJSON]) -> [Genre]? {
        
        var savedGenres = [Genre]()
        
        for genre in genres {
            
            let newGenre = Genre(context: PersistenceService.context)
            newGenre.setValue(genre.name, forKey: "name")
            newGenre.setValue(genre.id, forKey: "id")
            
            savedGenres.append(newGenre)
        }
        
        do {
            try PersistenceService.context.save()
            print("save genres")
            return savedGenres
        } catch {
            print("Saving error")
            return nil
        }
    }
    
    static func findGenres(completion: @escaping ([Genre]) -> ()) {
        
        let fetchedGenres = PersistenceService.fetchAllGenres()
        
        if fetchedGenres == nil || fetchedGenres!.isEmpty {
            getAllGenres(completion: { (genres) in
                print("genres from server")
                
                if let genres = genres {
                    completion(genres)
                }
            })
        } else {
            completion(fetchedGenres!)
        }
    }
    
    static func configGenrePicker(_ picker: UISegmentedControl) {
        
        findGenres { (genres) in
            DispatchQueue.main.async {
                for genre in genres where genres.count == picker.numberOfSegments {
                
                    print(genre)
                    picker.setTitle(genre.name, forSegmentAt: Int(genre.id) - 1)
                }
            }
        }
    }
}
