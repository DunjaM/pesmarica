//
//  API.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/5/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//

import UIKit

struct Group {
    static let genres = "genres"
    static let songs = "songs"
}

enum Method: String {
    case get = "GET"
    case post = "POST"
}

typealias CompletionHandler = (_ result: Data?, _ success: Bool) -> Void

class BaseAPI: NSObject {
    
    static let baseApi = "https://pesmarica-server.herokuapp.com/"
    
    static func sendRequest(_ service: String, method: Method, params: Dictionary<String, Any>?, completionHandler: @escaping CompletionHandler) {
        
        var request = URLRequest(url: URL(string: baseApi + service)!)
        
        print("\n API call to:")
        print("\(request.url!)\n")
        
        request.httpMethod = method.rawValue
        
        if params != nil {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params ?? [:], options: [])
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        }
        
        let session = URLSession.shared
        let task = session.dataTask(with: request) { data, response, error -> Void in
            
            if error == nil && data != nil {
                completionHandler(data, true)
            } else {
                completionHandler(nil, false)
            }
        }
        
        task.resume()
    }
}
