//
//  Song+CoreDataProperties.swift
//  Pesmarica
//
//  Created by Dunja Maksimovic on 12/7/17.
//  Copyright © 2017 Dunja Maksimovic. All rights reserved.
//
//

import Foundation
import CoreData


extension Song {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Song> {
        return NSFetchRequest<Song>(entityName: "Song")
    }

    @NSManaged public var genre_id: Int16
    @NSManaged public var singer: String?
    @NSManaged public var song_id: Int16
    @NSManaged public var title: String?
    @NSManaged public var genre: Genre?

}
